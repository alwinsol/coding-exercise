
# Coding Exercise Instructions #

* The goal of this exercise is to get an idea of the code you would produce if you were working at GoSweat.

## Your submission ##

* should be production quality: this is as important as solving the problem itself. 
* should be created using Java or Javascript 
* should be executable from the command line
* should provide a class for starting the application but you don't need to package the application. 
* should include unit tests.
* could provide a README, e.g. to clarify any assumption you have made. 

We want to make it easy for you to submit, and for us to review your submission so please follow these steps:
* Sign up for a free personal account on http://bitbucket.org.
* Create a private fork of https://bitbucket.org/gosweatbootcamp/coding-exercise.git   
* Commit and push your submission to your fork.
* Share your forked repository with the user gosweat_bootcamp
* We'll acknowledge receipt.


## Instructions to execute code.

Due to me going on holiday I have not really go stuck into this problem so I have just created a set of simple TDD tests, 
which may be expanded upon further.

To run the files, clone the files navigate to the jasmine standalone respoitory and from the command line run open SpecRunner.html

This will show the 4 simple passing tests.