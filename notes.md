# Notes

 * x, y co-ordinates

 * North - N

 * East - E

 * South - S

 * West - W

 * M - Move forward one grid point

 * L - Spin left

 * R - Spin right

 * Start position (0, 0)
 