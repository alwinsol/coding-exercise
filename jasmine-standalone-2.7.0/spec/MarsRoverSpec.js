describe ("MarsRover", function() {

  var x = 0;
  var y = 0;
  var direction = 'N';
  var vehicle = new MarsRover(x, y, direction);

  it("has a starting position", function(){
    expect(vehicle.location()).toEqual([x, y]);
  });

  it("can go in a specific direction", function(){
    expect(vehicle.direction()).toEqual(direction);
  });

  it("will throw an exception if X coordinate input is not an integer", function(){
    expect(new MarsRover('not_a_number', y, direction).location).toThrow();
  });

  it("will throw an exception if Y coordinate input is not an integer", function(){
    expect(new MarsRover(x, 'not_a_number', direction).location).toThrow();
  });

});
