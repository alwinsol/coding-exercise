function MarsRover(x, y, direction){

  this.x = x;
  this.y = y;

  this.location = function(){
    if (isNaN(this.x)) throw "X must be an integer";
    if (isNaN(this.y)) throw "Y must be an integer";
    return [x, y];
  };

  this.direction = function(){
    return direction;
  };

};
